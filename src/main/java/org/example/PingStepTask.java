package org.example;

public class PingStepTask extends StepTask implements Runnable {

    public PingStepTask(final ResourceHolder resourceHolder) {
        super(resourceHolder, App.PING, Worker.PING_WORKER);
    }

    @Override
    public void run() {
        try {
            doSteps();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
