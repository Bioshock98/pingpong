package org.example;

public enum Worker {

    PING_WORKER,
    PONG_WORKER,
    PRINT_WORKER
}
