package org.example;


public class App {

    public static final String PONG = "pong";
    public static final String PING = "ping";
    public static final int maxSteps = 4;
    public static final ResourceHolder resourceHolder = new ResourceHolder(maxSteps, Worker.PING_WORKER);

    public static void main(String[] args) throws InterruptedException {
        Thread pingThread = new Thread(new PingStepTask(resourceHolder));
        Thread pongThread = new Thread(new PongStepTask(resourceHolder));
        pingThread.start();
        pongThread.start();
        while(true) {
            synchronized(resourceHolder) {
                if(!Worker.PRINT_WORKER.equals(resourceHolder.getNextWorker())) {
                    resourceHolder.wait();
                }
                if(Worker.PRINT_WORKER.equals(resourceHolder.getNextWorker())) {
                    if(resourceHolder.getCounter() >= maxSteps) {
                        System.out.println(resourceHolder.getResource());
                        System.exit(0);
                        break;
                    }
                    final String resource = resourceHolder.getResource();
                    System.out.println(resource);
                    if(App.PING.equals(resource)) {
                        resourceHolder.setNextWorker(Worker.PONG_WORKER);
                    }
                    else {
                        resourceHolder.setNextWorker(Worker.PING_WORKER);
                    }
                    resourceHolder.notifyAll();
                }
            }
        }
    }
}
