package org.example;

public abstract class StepTask {

    private final ResourceHolder resourceHolder;
    private final String taskValue;
    private final Worker taskName;

    public StepTask(final ResourceHolder resourceHolder, final String taskValue, Worker taskName) {
        this.resourceHolder = resourceHolder;
        this.taskValue = taskValue;
        this.taskName = taskName;
    }

    protected void doSteps() throws InterruptedException {
        while(true) {
            synchronized(resourceHolder) {
                if(!taskName.equals(resourceHolder.getNextWorker())) {
                    resourceHolder.wait();
                }
                if(resourceHolder.getCounter() >= resourceHolder.getMaxSteps()) {
                    resourceHolder.notifyAll();
                    break;
                }
                if(taskName.equals(resourceHolder.getNextWorker())) {
                    resourceHolder.setResource(taskValue);
                    resourceHolder.setNextWorker(Worker.PRINT_WORKER);
                    resourceHolder.notifyAll();
                }
            }
        }
    }
}
