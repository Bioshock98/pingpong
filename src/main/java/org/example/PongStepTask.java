package org.example;

public class PongStepTask extends StepTask implements Runnable {

    public PongStepTask(final ResourceHolder resourceHolder) {
        super(resourceHolder, App.PONG, Worker.PONG_WORKER);
    }

    @Override
    public void run() {
        try {
            doSteps();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
